import {vdom1} from './fixtures/vdom1.js'
import {vdom2} from './fixtures/vdom2.js'

function renderNode (parent, node) {
  if ('textContent' in node) {
    const text = new window.Text(node.textContent)
    parent.append(text)
  }
  if ('tagName' in node) {
    const element = document.createElement(node.tagName)
    parent.append(element)
    if (Array.isArray(node.children)) {
      for (const child of node.children) {
        renderNode(element, child)
      }
    }
  }
}

renderNode(document.body, vdom1)

// diff(current, updated)

// Remove expired branches/nodes
// Add new branches/nodes
// Update changed nodes

// function updateTree (node1, node2) {
  if ('tagName' in node1) {
    const eol = Math.max(
      node1.children.length,
      node2.children.length
    )
    for (let i = 0; i < eol; i = i + 1) {
      const child1 = node1.children[i]
      const child2 = node1.children[i]
      // if no more child2 -> remove tree1 tail -> break
      // if no more child1 -> append tree2 tail -> break
      // if tagname differs -> replace child1 with child2 -> continue
    }
    // if (node1.tagName === node2.tagName) {
      // node1.children.forEach((node1Child, index) => {
      //   const hostChild = host.children[index]
      //   const node2Child = node2.children[index]
      //   updateTree(hostChild, node1Child, node2Child)
      // })
    // } else {
    //   host.remove()
    // }
  }

  if ('textContent' in node1) {
    if (node1.textContent === node2.textContent) {
      // Chillax
    } else {
      host.textContent = node2.textContent
    }
  }
}

updateTree(
  vdom1, vdom2
)

// Run instructions on DOM
// document.body.querySelector('div'),
