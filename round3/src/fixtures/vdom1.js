export const vdom1 = {
  // DocumentFragment
  tagName: 'div',
  children: [
    {
      // Test: Remove
      tagName: 'input'
    },
    {
      // Element
      tagName: 'ul',
      children: [
        {
          // Element
          tagName: 'li',
          children: [
            {

              // TextNode
              textContent: 'Hello, World!'
            }





          ]
        }
      ]
    }
  ]
}
