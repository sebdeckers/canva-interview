export const vdom2 = {
  // DocumentFragment
  tagName: 'div',
  children: [
    {




      // Element
      tagName: 'ul',
      children: [
        {
          // Element
          tagName: 'li',
          children: [
            {
              // Test: Update
              // TextNode
              textContent: 'Hello,'
            },
            {
              // Test: Add
              // TextNode
              textContent: 'World!'
            }
          ]
        },
        {
          // Test: Add
          // Element
          tagName: 'li',
          children: [
            {
              // TextNode
              textContent: 'Foo Bar'
            }
          ]
        }
      ]
    }
  ]
}
