# canva-interview 🥫

Scratchpad repo for interview rounds with Canva.

This repo does not contain a complete solutions. It also does not contain the intermediate steps. This is just the final, non-functioning source code I end up with, and my thoughts about the process in this document.

The aim of this repo is to share my feedback with Canva and record my own experience. Not to give away answers or provide future candidates with unfair advantages.

## Round 0

Introduced via email by an old friend who currently works at Canva.

## Round 1

Voice call with in-house recruiter.

Relocation to Sydney required. Acceptable timeframe is generous: up to end of year (from January).

## Round 2

Live-coding challenges using Hacker Trail IDE web app and a video/voice call.

Tested knowledge of JSON, fetch, and abilisty to recursively process a node list to "average" RGB values.

Retrospective:

- Highlighted issues with the Hacker Trail IDE by pointed out the exact version of Node.js it runs on, and compared my accustomed coding style (async/await) with the supported older style (then/catch). Hope I didn't sound arrogant/entitled; just curious about how systems work.

- Recursion trips me up. My brain goes into ultra slow-and-careful mode. Due to time constraint we did one final iteration of the problem purely as a thought-exercise. Refactoring the code would be a low value-add exercise at that point.

## Round 3

The interview was designed to examine "theoretical reasoning" skills. The aim is not just working code, though I was told that would be nice to have.

The challenge involved some tree data structure design & tree diff algorithm.

I got stuck on the recursion logic. Turns out this is a typical Google-esque interview question that CS grads know by heart and the standard response would be to use a queue or stack to avoid the iteration complexity. Oh well... "theoretical" 🙄

I believe my strenths were:
- reasoning about, and demonstrating my knowledge of, the DOM APIs (e.g. DOM4 remove/append, TreeWalker)
- considering the performance implications of different design trade-offs
- identifying parts of the problem that can be isolated
- approaching the problem from different angles: resursion vs flattening, direct manipulation vs generating instructions, processing 3 vs 2 trees, etc.
- questioning interviewer assumptions and incremental goals
- minimalist data/test-first approach to avoid inflating the problem
- using various supporting tools: standardjs/eslint, http2 server, localhost tls, es modules with cors, webkit debugger, etc.

## Round 4

Briefing: Emphasis on actual coding. Implement a lightweight Canva UI. Given some SVG assets, something about drag-and-drop, involves coding a game (misheard?), etc.

Potential relevant experience to review:
- Drywall (2014-2015) heavily uses SVG and JS/CSS animation with GSAP,and Backbone.js
  - Demo: https://drywall.cf.sg
  - Code: https://github.com/drywallio
- CSSConf.Asia 2014 talk about SVG
  - https://www.youtube.com/watch?v=VQ7_MwjPqKs&index=2&list=PL37ZVnwpeshHFbT0mLTNMtMGO1mo6yPRX
